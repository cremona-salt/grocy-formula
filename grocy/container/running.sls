# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_file = tplroot ~ '.config.file' %}
{%- from tplroot ~ "/map.jinja" import grocy with context %}

{%- set mountpoint = grocy.docker.mountpoint %}

include:
  - {{ sls_config_file }}

grocy-container-running-container-present:
  docker_image.present:
    - name: {{ grocy.container.image }}
    - tag: {{ grocy.container.tag }}
    - force: True

grocy-container-running-container-managed:
  docker_container.running:
    - name: {{ grocy.container.name }}
    - image: {{ grocy.container.image }}:{{ grocy.container.tag }}
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/grocy/config:/config
    - port_bindings:
      - 9283:80
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.grocy-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.grocy-web.middlewares=grocy-redirect-websecure"
      - "traefik.http.routers.grocy-web.rule=Host(`{{ grocy.container.url }}`)"
      - "traefik.http.routers.grocy-web.entrypoints=web"
      - "traefik.http.routers.grocy-websecure.rule=Host(`{{ grocy.container.url }}`)"
      - "traefik.http.routers.grocy-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.grocy-websecure.tls=true"
      - "traefik.http.routers.grocy-websecure.entrypoints=websecure"

grocy-container-running-barcodebuddy-container-present:
  docker_image.present:
    - name: {{ grocy.barcodebuddy.image }}
    - tag: {{ grocy.barcodebuddy.tag }}
    - force: True

grocy-container-running-barcodebuddy-container-managed:
  docker_container.running:
    - name: {{ grocy.barcodebuddy.name }}
    - image: {{ grocy.barcodebuddy.image }}:{{ grocy.barcodebuddy.tag }}
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/barcodebuddy/config:/config
    - port_bindings:
      - 9020:80
      - 9021:443
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.barcodebuddy-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.barcodebuddy-web.middlewares=barcodebuddy-redirect-websecure"
      - "traefik.http.routers.barcodebuddy-web.rule=Host(`{{ grocy.barcodebuddy.url }}`)"
      - "traefik.http.routers.barcodebuddy-web.entrypoints=web"
      - "traefik.http.routers.barcodebuddy-websecure.rule=Host(`{{ grocy.barcodebuddy.url }}`)"
      - "traefik.http.routers.barcodebuddy-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.barcodebuddy-websecure.tls=true"
      - "traefik.http.routers.barcodebuddy-websecure.entrypoints=websecure"
