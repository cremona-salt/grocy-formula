# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import grocy with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = grocy.docker.mountpoint %}

{%- for dir in [
  'docker/grocy/config',
  'docker/barcodebuddy/config',
  ]
%}

grocy-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: stooj
    - group: users
    - dir_mode: 0755
    - file_mode: 0644
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}
